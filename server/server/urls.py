from django.contrib import admin
from django.urls import path

from color.views import generate_new_color
from sine.views import generate_new_sine

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/colors/generate-new-color', generate_new_color),
    path('api/sines/generate-new-sine', generate_new_sine),
]
